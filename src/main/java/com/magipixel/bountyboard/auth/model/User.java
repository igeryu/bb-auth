package com.magipixel.bountyboard.auth.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class User {
    private String username;
    private String password;
    private String role;
}