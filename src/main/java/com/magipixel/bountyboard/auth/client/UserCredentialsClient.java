package com.magipixel.bountyboard.auth.client;

import com.magipixel.bountyboard.auth.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@FeignClient(
        url = "${credentials-service.hostname}",
        decode404 = true,
        name = "user-credentials",
        path = "/users"
)
public interface UserCredentialsClient {

    @GetMapping("/search/findByUsername")
    Optional<User> findByUsername(@RequestParam final String username);
}