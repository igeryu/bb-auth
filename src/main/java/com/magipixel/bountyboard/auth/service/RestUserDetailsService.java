package com.magipixel.bountyboard.auth.service;

import com.magipixel.bountyboard.auth.client.UserCredentialsClient;
import com.magipixel.bountyboard.auth.model.User;
import com.magipixel.bountyboard.auth.userdetails.MyUserPrincipal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class RestUserDetailsService implements UserDetailsService {
    private final UserCredentialsClient userCredentialsClient;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        log.info("Looking for credentials for email: {}", username);
        final Optional<User> userOptional = userCredentialsClient.findByUsername(username);

        if (userOptional.isPresent()) {
            log.info("Credentials found for email: {}", username);
            return new MyUserPrincipal(userOptional.get());
        } else {
            log.warn("Credentials not found for email: {}", username);
            throw new UsernameNotFoundException(username);
        }
    }
}