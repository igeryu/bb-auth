package com.magipixel.bountyboard.auth;

import com.magipixel.bountyboard.auth.client.UserCredentialsClient;
import com.magipixel.bountyboard.auth.model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Base64;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = AuthServiceApplication.class)
@AutoConfigureMockMvc
@Tag("integration")
class AuthorizationIT {
    @Value("${security.oauth2.client.client-id}")
    private String clientId;

    @Value("${security.oauth2.client.client-secret}")
    private String clientSecret;

    @MockBean
    private UserCredentialsClient userCredentialsClient;

    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Autowired
    private MockMvc mockMvc;

    @Test()
    @DisplayName("Given valid credentials, When authenticating, Then succeed")
    void givenValidCredentials_whenAuthenticating_thenSucceed() throws Exception {
        // Arrange
        final String idSecret = clientId + ":" + clientSecret;
        final String basicAuthToken = Base64.getEncoder().encodeToString(idSecret.getBytes());
        final String username = "can_be_anything_1234";
        final String password = "1234_can_be_anything";
        final String encryptedPassword = bcrypt.encode(password);
        final User userCredentials = new User();
        userCredentials.setUsername(username);
        userCredentials.setPassword(encryptedPassword);
        userCredentials.setRole("role");
        given(userCredentialsClient.findByUsername(username)).willReturn(Optional.of(userCredentials));

        // Act / Assert
        mockMvc.perform(post("/oauth/token")
                .header("Authorization", "Basic " + basicAuthToken)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("grant_type=password&username=" + username + "&password=" + password))
                .andExpect(status().isOk());
    }

    @Test()
    @DisplayName("Given invalid password, When authenticating, Then fail")
    void givenInvalidPassword_whenAuthenticating_thenFail() throws Exception {
        // Arrange
        final String idSecret = clientId + ":" + clientSecret;
        final String basicAuthToken = Base64.getEncoder().encodeToString(idSecret.getBytes());
        final String username = "can_be_anything_1234";
        final String password = "1234_can_be_anything";
        final String encryptedPassword = bcrypt.encode("some other password");
        final User userCredentials = new User();
        userCredentials.setUsername(username);
        userCredentials.setPassword(encryptedPassword);
        given(userCredentialsClient.findByUsername(username)).willReturn(Optional.of(userCredentials));

        // Act / Assert
        mockMvc.perform(post("/oauth/token")
                .header("Authorization", "Basic " + basicAuthToken)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("grant_type=password&username=" + username + "&password=" + password))
                .andExpect(status().isBadRequest());
    }

    @Test()
    @DisplayName("Given invalid username, When authenticating, Then fail")
    void givenInvalidUsername_whenAuthenticating_thenFail() throws Exception {
        // Arrange
        final String idSecret = clientId + ":" + clientSecret;
        final String basicAuthToken = Base64.getEncoder().encodeToString(idSecret.getBytes());
        final String username = "can_be_anything_1234";
        final String password = "1234_can_be_anything";
        given(userCredentialsClient.findByUsername(username)).willReturn(Optional.empty());

        // Act / Assert
        mockMvc.perform(post("/oauth/token")
                .header("Authorization", "Basic " + basicAuthToken)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("grant_type=password&username=" + username + "&password=" + password))
                .andExpect(status().isBadRequest());
    }


}