package com.magipixel.bountyboard.auth.model;

import com.magipixel.bountyboard.auth.MockitoStrictStubbingTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class UserTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private User user;

    @Test
    void getUsername() {
        // Arrange
        final String expectedUsername = "expected_userName";

        // Act
        user.setUsername(expectedUsername);

        // Assert
        assertThat(user.getUsername(), is(equalTo(expectedUsername)));
    }

}