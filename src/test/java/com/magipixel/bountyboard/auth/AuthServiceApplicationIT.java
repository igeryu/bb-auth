package com.magipixel.bountyboard.auth;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.OrderingComparison.greaterThan;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Tag("integration")
@RequiredArgsConstructor
class AuthServiceApplicationIT {

    private final ApplicationContext context;

    @Test
    void contextLoads() {
        assertThat(context.getBeanDefinitionCount(), is(greaterThan(0)));
    }

}