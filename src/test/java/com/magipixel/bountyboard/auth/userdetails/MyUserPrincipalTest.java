package com.magipixel.bountyboard.auth.userdetails;

import com.magipixel.bountyboard.auth.MockitoStrictStubbingTest;
import com.magipixel.bountyboard.auth.model.User;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class MyUserPrincipalTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private MyUserPrincipal myUserPrincipal;

    @Test
    void getAuthorities() {
        // Arrange
        final String expectedRole = "expected_role";
        final Collection<? extends GrantedAuthority> expectedAuthorities =
                Collections.singletonList(new SimpleGrantedAuthority(expectedRole));
        final User user = new User();
        user.setRole(expectedRole);

        // Act
        myUserPrincipal = new MyUserPrincipal(user);

        // Assert
        assertThat(myUserPrincipal.getAuthorities(), is(equalTo(expectedAuthorities)));
    }

    @Test
    void getUsername() {
        // Arrange
        final String expectedUsername = "expected_username";
        final User user = new User();
        user.setUsername(expectedUsername);

        // Act
        myUserPrincipal = new MyUserPrincipal(user);

        // Assert
        assertThat(myUserPrincipal.getUsername(), is(equalTo(expectedUsername)));
    }

}