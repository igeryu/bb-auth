package com.magipixel.bountyboard.auth.service;

import com.magipixel.bountyboard.auth.client.UserCredentialsClient;
import com.magipixel.bountyboard.auth.model.User;
import com.magipixel.bountyboard.auth.userdetails.MyUserPrincipal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@Tag("unit")
class RestUserDetailsServiceTest {
    @InjectMocks
    private RestUserDetailsService service;

    @Mock
    private UserCredentialsClient credentialsClient;

    @Test
    @DisplayName("Given User exists, when loadByUsername, then return User")
    void givenUserExistsWhenLoadByUsernameThenReturnUser() {
        final User expectedUser = new User();
        expectedUser.setPassword("can_be_anything_1234");
        expectedUser.setUsername("can_be_anything_6789");
        given(credentialsClient.findByUsername(anyString())).willReturn(Optional.of(expectedUser));

        final UserDetails result = service.loadUserByUsername("abc123");

        assertThat(result, equalTo(new MyUserPrincipal(expectedUser)));
    }

    @Test
    @DisplayName("Given User does not exist, when loadByUsername, then throw UsernameNotFoundException")
    void givenUserDoesNotExistWhenLoadByUsernameThenThrowUsernameNotFoundException() {
        final User expectedUser = new User();
        expectedUser.setPassword("can_be_anything_1234");
        expectedUser.setUsername("can_be_anything_6789");
        given(credentialsClient.findByUsername(anyString())).willReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class,
                () -> service.loadUserByUsername("abc123")
        );

    }

}