package com.magipixel.bountyboard.auth.service;

import com.magipixel.bountyboard.auth.client.UserCredentialsClient;
import com.magipixel.bountyboard.auth.model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Tag("integration")
class RestUserDetailsServiceIT {
    @Autowired(required = false)
    private RestUserDetailsService service;

    @MockBean
    private UserCredentialsClient client;

    @Autowired(required = false)
    private PasswordEncoder encoder;

    @Test
    @DisplayName("Service is a Bean")
    void serviceIsABean() {
        assertThat(service, notNullValue());
    }

    @Test
    @DisplayName("UserCredentialsClient is @Autowired into Service")
    void credentialsClientIsAutowired() {
        given(client.findByUsername(anyString())).willReturn(Optional.of(new User()));

        service.loadUserByUsername("abc123");
    }

    @Test
    @DisplayName("BCryptPasswordEncoder is a Bean")
    void bCryptPasswordEncoderIsABean() {
        assertThat(encoder, notNullValue());
    }
}